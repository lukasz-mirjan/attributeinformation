<?php

class AttributeInformation extends ObjectModel
{
	public $id_attribute_information;
    public $fabric_name;
	public $fabric_informations;

/**
  * @see ObjectModel::$definition
 */
    public static $definition = array(
        'table' => 'attribute_information',
        'primary' => 'id_attribute_information',
        'multilang' => false,
        'fields' => array(
            'fabric_name' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
			'fabric_description' => array('type' => self::TYPE_HTML, 'validate' => 'isString'),
			'fabric_informations' => array('type' => self::TYPE_HTML, 'validate' => 'isString'),
        ),
    );
}